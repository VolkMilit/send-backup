#ifndef DATABASE_H
#define DATABASE_H

#include <string>
#include <list>

using namespace std;

class Database
{
public:
    Database();

    void write(const string &value);
    bool exist(const string &value) const;

private:
    void read();

    static inline string s_base_path = "/var/cache/ew-backups-send.list";
    list<string> m_base;
};

#endif // DATABASE_H
