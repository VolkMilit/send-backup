#include "third-party/cruel/cruel.h"
#include "third-party/ftlip/ftlip.h"

#include <filesystem>

#include "database.h"

namespace fs = std::filesystem;

int main()
{
    if (!fs::exists("/etc/telegram-bot-settings.conf"))
    {
        cout << "No configuration file found! Abort.\n";
        return -1;
    }

    ftlip ini("/etc/telegram-bot-settings.conf");

    const string backups_directory = ini.value("backups_directory").to_string();
    if (!fs::exists(backups_directory))
    {
        cout << "No directory named \"" + backups_directory + "\" found!\n";
        return -1;
    }

    const string ini_port = ini.value("port").to_string();
    string port;
    if (!ini_port.empty())
        port = ":" + ini_port;

    Cruel cruel(ini.value("addr").to_string() + port +
                "/bot" + ini.value("token").to_string() + "/sendDocument");

    if (!cruel.has_error())
    {
        const vector<pair<string, string>> data = {
            {"chat_id", "-100" + ini.value("chat_id").to_string()}
        };

        Database dbase;

        for (const auto &entity : fs::directory_iterator(backups_directory))
        {
            if (dbase.exist(entity.path().filename()))
                continue;

            const vector<pair<string, string>> files = {
                {"document", entity.path()}
            };

            cruel.send_form(data, files);

            if (cruel.has_error())
            {
                cout << "FATAL: " << cruel.error() << "\n";
                break;
            }
            else
            {
                const json response = cruel.response().to_json();
                if (!response["ok"])
                {
                    cout << "FATAL: Telegram reponse: " << response["description"] << "\n";
                    break;
                }
            }

            dbase.write(entity.path().filename());
        }
    }

    return 0;
}
