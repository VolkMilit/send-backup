#include "database.h"

#include <list>
#include <filesystem>
#include <fstream>
#include <algorithm>

namespace fs = std::filesystem;

Database::Database()
{
    if (!fs::exists(s_base_path))
        write("");
    else
        read();
}

void Database::write(const string &value)
{
    if (exist(value))
        return;

    ofstream file(s_base_path, ios::app);

    if (!file.is_open())
        throw runtime_error("Can't write to database!\n");

    file << value + "\n";

    file.close();
}

void Database::read()
{
    ifstream file(s_base_path);

    if (!file.is_open())
        throw runtime_error("Can't open database!\n");

    string line;
    while(getline(file, line))
        m_base.emplace_back(line);
}

bool Database::exist(const string &value) const
{
    auto it = find(m_base.begin(), m_base.end(), value);
    return it != m_base.end();
}
