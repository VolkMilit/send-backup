cmake_minimum_required(VERSION 3.5)

project(send-backups LANGUAGES CXX)

option(BUILD_STATIC OFF)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

if(BUILD_STATIC)
    if(WIN32)
        set(CMAKE_CXX_FLAGS "/MT")
    else()
        set(CMAKE_CXX_FLAGS "-static")
    endif()
endif()

include(third-party/cruel/cruel.cmake)

set(SOURCES
    main.cpp
    database.h
    database.cpp
    third-party/cruel/cruel.h
    third-party/ftlip/ftlip.h
)

add_executable(${PROJECT_NAME} ${SOURCES})
target_link_libraries(${PROJECT_NAME} ${CURL_LIBRARY})
